#include "hwSetup.h"
#include "debug.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"

/**
 * @brief
 *
 */
static void configSpi1(void) {
    // GPIO SPI1
    GPIO_InitTypeDef GPIO_InitStructure;

    GPIO_StructInit(&GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_7 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;

    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;

    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // CS config
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_WriteBit(GPIOB, GPIO_Pin_9, 1);

    SPI_InitTypeDef SPI_InitStructure;
    SPI_StructInit(&SPI_InitStructure);
    /* SPI Config
     * -------------------------------------------------------------*/
    SPI_InitStructure.SPI_Direction         = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode              = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize          = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL              = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA              = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS               = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_InitStructure.SPI_FirstBit          = SPI_FirstBit_MSB;
    // SPI_InitStructure.SPI_CRCPolynomial     = 7;
    SPI_Init(SPI1, &SPI_InitStructure);

    /* Enable SPIy */
    SPI_Cmd(SPI1, ENABLE);
}

/**
 * @brief
 *
 */
static void RCC_Config(void) {

    /* Enable peripheral clocks
     * --------------------------------------------------*/

    /* Enable SPIy clock and GPIO clock for SPIy and SPIz */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_SPI1 |
                               RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOA |
                               RCC_APB2Periph_GPIOC,
                           ENABLE);
}

/**
 * @brief
 *
 * @return true
 * @return false
 */
bool hwSetup(void) {
    bool res = false;

    SystemCoreClockUpdate();

    RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq(&RCC_Clocks);

    // Print clocks and stack and heap size
    DEBUG_MSG("============ STM32F103C6T6 ============\n");
    DEBUG_MSG("HEAP size :%d bytes\n", __HEAP_SIZE);
    DEBUG_MSG("STACK size:%d bytes\n", __STACK_SIZE);
    DEBUG_MSG("\n");
    DEBUG_MSG("SYSCLK :%ld MHz\n", RCC_Clocks.SYSCLK_Frequency / 1000000);
    DEBUG_MSG("HCLK   :%ld MHz\n", RCC_Clocks.HCLK_Frequency / 1000000);
    DEBUG_MSG("PCLK1  :%ld MHz\n", RCC_Clocks.PCLK1_Frequency / 1000000);
    DEBUG_MSG("PCLK2  :%ld MHz\n", RCC_Clocks.PCLK2_Frequency / 1000000);
    DEBUG_MSG("ADCCLK :%ld MHz\n", RCC_Clocks.ADCCLK_Frequency / 1000000);
    DEBUG_MSG("=======================================\n");

    RCC_Config();

    // System LED config PC13
    GPIO_InitTypeDef gpioInit;

    GPIO_StructInit(&gpioInit);

    gpioInit.GPIO_Pin   = GPIO_Pin_13;
    gpioInit.GPIO_Mode  = GPIO_Mode_Out_PP;
    gpioInit.GPIO_Speed = GPIO_Speed_10MHz;

    GPIO_Init(GPIOC, &gpioInit);

    configSpi1();

    return res;
}
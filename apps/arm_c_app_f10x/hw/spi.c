#include "stm32f10x_conf.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_spi.h"

#include "spi.h"

uint8_t spiTxRx(uint8_t data) {
    uint8_t rxData;

    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET)
        ;
    SPI_I2S_SendData(SPI1, data);

    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE) == RESET)
        ;
    rxData = (uint8_t)(SPI_I2S_ReceiveData(SPI1) & 0x00ff);

    return rxData;
}
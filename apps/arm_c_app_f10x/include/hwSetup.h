#ifndef HWSETUP_H
#define HWSETUP_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SYSTEM_LED_PORT GPIOC
#define SYSTEM_LED_PIN GPIO_Pin_13

bool hwSetup(void);

#ifdef __cplusplus
}
#endif

#endif
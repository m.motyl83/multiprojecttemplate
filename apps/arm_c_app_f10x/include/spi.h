#ifndef SPI_H
#define SPI_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint8_t spiTxRx(uint8_t data);

#ifdef __cplusplus
}
#endif

#endif
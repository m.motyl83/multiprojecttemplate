#include "dev_io.h"
#include "io_hw_proxy.h"
#include "stm32f10x_it.h"
#include <FreeRTOS.h>
#include <errno.h>
#include <misc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/unistd.h>
#include <task.h>
#include <time.h>

/**
 * Functions in this file are needed when we use original printf from c library
 * for exaple printf
 */

int _kill(int pid, int sig);
int _getpid(void);
int _open(const char *name, int flags, int mode);
int _gettimeofday(struct timeval *tv, struct timezone *tz);
int _read(int file, char *ptr, int len);
int _close(int file);
int _fstat(int file, struct stat *st);
int _isatty(int file);
int _lseek(int file, int ptr, int dir);
int _write(int file, char *ptr, int len);
int fcntl(int fd, int cmd, ...);
caddr_t _sbrk(int incr);

extern void __io_putchar(char ch) __attribute__((weak));
extern char __io_getchar(void) __attribute__((weak));

// register char *stack_ptr asm("sp");

#ifndef SEMIHOSTING
/**
 * @brief
 *
 * @return int
 */
int _getpid(void) {
    return 1;
}

/**
 * @brief
 *
 * @param pid
 * @param sig
 * @return int
 */
int _kill(int pid, int sig) {
    (void)pid;
    (void)sig;
    errno = EINVAL;
    return -1;
}

/**
 * @brief
 *
 * @param name
 * @param flags
 * @param mode
 * @return int
 */
int _open(const char *name, int flags, int mode) {
    size_t nameLen = strnlen(name, 16);
    (void)mode;
    (void)flags;

    if (strncmp(name, "/dev/spi", nameLen) == 0) {
        return SPI_DEV_FILENO;
    } else {
        if (strncmp(name, "/dev/i2c", nameLen) == 0) {
            return I2C_DEV_FILENO;
        } else {
            if (strncmp(name, "/dev/gpio", nameLen) == 0) {
                return GPIO_DEV_FILENO;
            } else {
                if (strncmp(name, "/dev/uart", nameLen) == 0) {
                    return UART_DEV_FILENO;
                }
            }
        }
    }

    return -ENODEV;
}

/**
 * @brief
 *
 * @param status
 */
void _exit(int status) {
    _kill(status, -1);
    while (1) {
    } /* Make sure we hang here */
}

/**
 * @brief
 *
 * @param tv
 * @param tz
 * @return int
 */
int _gettimeofday(struct timeval *tv, struct timezone *tz) {
    (void)tz;

    tv->tv_sec  = xTaskGetTickCount() / 1000;
    //tv->tv_usec = (long int)(tv->tv_sec % 1000) / 1000;
    tv->tv_usec = 0;

    return 0;
}

/**
 * @brief
 *
 * @param file
 * @param ptr
 * @param len
 * @return int
 */
int _read(int file, char *ptr, int len) {

    if (len <= 0 || ptr == NULL || file < 0)
        return -EINVAL;
        
    int DataIdx = 0;

    switch (file) {
    case STDIN_FILENO:
        while (1) {
            *ptr = __io_getchar();

            if (*ptr == '\n' || *ptr == '\r')
                return DataIdx;

            ptr++;
            DataIdx++;
        }

        return DataIdx;
        break;

    default:
        return devRead(file, ptr, len);
    }
}

/**
 * @brief
 *
 * @param file
 * @return int
 */
int _close(int file) {
    (void)file;
    return -1;
}

/**
 * @brief  Status of an open file. For consistency with other minimal
 * implementations in these examples, all files are regarded as character
 * special devices. The sys/stat.h' header file required is distributed in the
 * `include' subdirectory for this C library.
 *
 * @param file
 * @param st
 * @return int
 */
int _fstat(int file, struct stat *st) {
    (void)file;
    st->st_mode = S_IFCHR;
    return 0;
}

/**
 * @brief  Query whether output stream is a terminal. For consistency with the
 * other minimal implementations,
 *
 * @param file
 * @return int
 */
int _isatty(int file) {
    switch (file) {
    case STDOUT_FILENO:
    case STDERR_FILENO:
    case STDIN_FILENO:
        return 1;
    default:
        // errno = ENOTTY;
        errno = EBADF;
        return 0;
    }
}

/**
 * @brief Set position in a file. Minimal implementation:
 *
 * @param file
 * @param ptr
 * @param dir
 * @return int
 */
int _lseek(int file, int ptr, int dir) {
    (void)file;
    (void)ptr;
    (void)dir;
    return 0;
}

/**
 * @brief
 *
 * @param file
 * @param ptr
 * @param len
 * @return int
 */
int _write(int file, char *ptr, int len) {

    if (len <= 0 || ptr == NULL || file < 0)
        return -EINVAL;

    switch (file) {
    case STDOUT_FILENO:
    case STDERR_FILENO:

        for (int DataIdx = 0; DataIdx < len; DataIdx++) {
            __io_putchar(*ptr++);
        }

        return len;
        break;

    default:
        return devWrite(file, ptr, len);
    }
}
#endif

#ifndef FreeRTOS
register char *stack_ptr asm("sp");
#endif

/**
 * @brief
 *
 * @param incr
 * @return caddr_t
 */
caddr_t _sbrk(int incr) {
    extern char end asm("end");
    static char *heap_end;
    char *prev_heap_end;

    if (heap_end == 0) {
        heap_end = &end;
    }

    prev_heap_end = heap_end;

#ifdef FreeRTOS
    char *min_stack_ptr;
    /* Use the NVIC offset register to locate the main stack pointer. */
    min_stack_ptr = (char *)(*(unsigned int *)*(unsigned int *)0xE000ED08);
    /* Locate the STACK bottom address */
    min_stack_ptr -= MAX_STACK_SIZE;

    if (heap_end + incr > min_stack_ptr)
#else
    if (heap_end + incr > stack_ptr)
#endif
    {
        //		write(1, "Heap and stack collision\n", 25);
        //		abort();
        errno = ENOMEM;
        return (caddr_t)-1;
    }

    heap_end += incr;

    return (caddr_t)prev_heap_end;
}

/**
 * @brief This function can be used to set options of HW, like setting GPIO
 * interrupt callback
 *
 * @param fd
 * @param cmd
 * @param ...
 * @return int
 */
int fcntl(int fd, int cmd, ...) {
    (void)fd;
    (void)cmd;
    asm volatile("nop");
    return 0;
}
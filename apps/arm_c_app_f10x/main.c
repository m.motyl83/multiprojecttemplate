#include "debug.h"
#include "FreeRTOS.h"
#include "hwSetup.h"
#include "io_hw_proxy.h"
#include "spi.h"
#include "task.h"
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

void vTask(void *pvParams);
void vTask2(void *pvParams);

uint8_t spiTxRx(uint8_t data);
static uint32_t foo = 0;

void ledCtrl(bool lit);

int main(void) {
#ifdef ENABLE_UART_DEBUG
    USARTdebugInit();
#endif

#ifdef SEMIHOSTING
    initialise_monitor_handles();
#endif

    // fcntl(STDOUT_FILENO, 0, 0);

    hwSetup();

    BaseType_t res;
    res = xTaskCreate(vTask, "T1", 256, NULL, 1, NULL);
    DEBUG_MSG("xTaskCreate res:%ld\n", res);
    res = xTaskCreate(vTask2, "T2", 256, NULL, 1, NULL);
    DEBUG_MSG("xTaskCreate res:%ld\n", res);

    vTaskStartScheduler();

    return 0;
}

void vTask(void *pvParams) {
    int counter = 0;
    uint8_t val = 1;
    uint8_t shf = 0;

    UNUSED(pvParams);

    int spiFd = open("/dev/spi", O_RDWR);

    io_hw_proxy_desc_t f = {0};

    f.devData.spiDevData.csPort   = _GPIOB;
    f.devData.spiDevData.csPin    = 9;
    f.devData.spiDevData.assertCS = true;
    f.devData.spiDevData.devId    = _SPI1;

    f.data    = &val;
    f.dataLen = 1;

    for (;;) {
        struct timeval tv;
        gettimeofday(&tv ,NULL);
        foo++;
        DEBUG_MSG("Seconds Count: %d sec:%ld  usec:%ld\n", counter++, (long int)tv.tv_sec, (long int)tv.tv_usec);

        val = (1 << shf);

        write(spiFd, &f, sizeof(io_hw_proxy_desc_t));

        shf++;
        shf = shf % 8;

        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

void vTask2(void *pvParams) {
    UNUSED(pvParams);
    bool ledState = false;
    for (;;) {

        ledCtrl(ledState);

        ledState = !ledState;

        vTaskDelay(110 / portTICK_PERIOD_MS);
    }
}

void ledCtrl(bool lit) {
    io_hw_proxy_desc_t f;

    f.devData.gpioDevData.devId   = _GPIOC;
    f.devData.gpioDevData.pin     = 13;
    f.devData.gpioDevData.portVal = 0;
    f.devData.gpioDevData.pinVal  = (uint8_t)lit;

    int devFd = open("/dev/gpio", O_RDWR);

    int res = write(devFd, &f, sizeof(io_hw_proxy_desc_t));

    UNUSED(res);

    close(devFd);
}

cmake_minimum_required(VERSION 3.5)

include("${CMAKE_SOURCE_DIR}/cmake/functions.cmake")

enable_language(CXX)

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})

set(BINARY_NAME ${ProjectId})

set(SOURCES "main.cpp")
set(LINKER_MAP_FILE "${CMAKE_SOURCE_DIR}/linker/${BINARY_NAME}.map")

set(CXX_BUILD_OPTS
    -Werror
    -Wall
    -Wmissing-declarations
    -Wpointer-arith
    -Wwrite-strings
    -Wcast-qual
    -Wcast-align
    -Wformat-security
    -Wformat-nonliteral
    -Wmissing-format-attribute
    -Winline)

add_compile_options(-Wformat -Wformat-security)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  # SET(CMAKE_C_FLAGS " ${CMAKE_C_FLAGS} -g3") add_compile_options()
endif()

set(BINARY_PATH "${CMAKE_BINARY_DIR}/${BINARY_NAME}")

if(CMAKE_BUILD_TYPE STREQUAL "Release")
  set(CMAKE_EXE_LINKER_FLAGS " ${CMAKE_EXE_LINKER_FLAGS} -s")
endif()

if(CMAKE_BUILD_TYPE STREQUAL "MinSizeRel")
  set(CMAKE_ASM_FLAGS_MINSIZEREL " ${CMAKE_ASM_FLAGS_MINSIZEREL} ")
endif()

if(CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
  set(CMAKE_ASM_FLAGS_RELWITHDEBINFO " ${CMAKE_ASM_FLAGS_RELWITHDEBINFO} ")
endif()

include_directories(${CMAKE_SOURCE_DIR}/include ${INCLUDES}
                    ${CMAKE_SOURCE_DIR}/lib/foo)

link_directories(${PROJECT_SOURCE_DIR}/lib)

add_executable(${BINARY_NAME} ${SOURCES} ${INCLUDES})

target_compile_options(${BINARY_NAME}
                       PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${CXX_BUILD_OPTS}>)

target_link_libraries(${BINARY_NAME} PRIVATE foo)
target_link_options(${BINARY_NAME} PRIVATE -Xlinker -Map=${BINARY_NAME}.map)

add_dependencies(${BINARY_NAME} foo)

# add_custom_target(doxygen_doc COMMAND doxygen
# ${CMAKE_SOURCE_DIR}/doxygen.config)
configure_file(${CMAKE_SOURCE_DIR}/cmake/global_defines.h.in
               ${CMAKE_SOURCE_DIR}/include/global_defines.h @ONLY)

add_custom_command(
  TARGET ${BINARY_NAME}
  POST_BUILD
  COMMAND size ${CMAKE_CURRENT_BINARY_DIR}/${BINARY_NAME})

#include "FreeRTOS.h"
#include "debug.h"
#include "io_hw_proxy.h"
#include "misc.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "task.h"
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void vTask(void *pvParams);
void vTask2(void *pvParams);

int main(void) {
#ifdef ENABLE_UART_DEBUG
    USARTdebugInit();
    DEBUG_MSG("Hello world! from printf\n");
#endif

#ifdef SEMIHOSTING
    initialise_monitor_handles();
#endif

    io_hw_proxy_desc_t f;
    
    f.devData.gpioDevData.devId = 0;
    f.devData.gpioDevData.pin    = 1;
    f.devData.gpioDevData.port   = 2;

    int devFd = open("/dev/gpio", O_RDWR);

    int res = write(devFd, &f, sizeof(io_hw_proxy_desc_t));

    UNUSED(res);

    fcntl(STDOUT_FILENO, 0, 0);

    SystemCoreClockUpdate();
    RCC_ClocksTypeDef RCC_Clocks;
    RCC_GetClocksFreq(&RCC_Clocks);

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);

    GPIO_InitTypeDef gpioInit;

    GPIO_StructInit(&gpioInit);

    gpioInit.GPIO_Pin   = GPIO_Pin_13;
    gpioInit.GPIO_Mode  = GPIO_Mode_OUT;
    gpioInit.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    gpioInit.GPIO_OType = GPIO_OType_PP;

    GPIO_Init(GPIOC, &gpioInit);

    xTaskCreate(vTask, "T1", 150, NULL, 1, NULL);
    xTaskCreate(vTask2, "T2", 150, NULL, 1, NULL);

    vTaskStartScheduler();

    return 0;
}

void vTask(void *pvParams) {
    int counter = 0;
    DEBUG_MSG("Clock Task Start Running...\n");

    for (;;) {
        DEBUG_MSG("Seconds Count: %d\n", counter++);
        /*
           create 1-second (1000-msec) delay.
        */
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
}

void vTask2(void *pvParams) {
    for (;;) {
        GPIO_ToggleBits(GPIOC, GPIO_Pin_13);
        /*
           create 1-second (1000-msec) delay.
        */
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

#include "global_defines.h"
#include <stdio.h>

#define UNUSED(x) (void)(x)

#define STRINGIFY(s) XSTRINGIFY(s)
#define XSTRINGIFY(s) #s

#ifdef ENABLE_UART_DEBUG

#if 0
#pragma message(                                                               \
    "Setting SERIAL_PORT_BAUDRATE is " STRINGIFY(UART_DEBUG_SPEED) "bps")
#endif

#define DEBUG_MSG(fmt, args...)                                                \
    printf("[DEBUG]:" fmt, ##args) /*info function */
#else
#define DEBUG_MSG(fmt, args...)
#endif

void USARTdebugInit(void);

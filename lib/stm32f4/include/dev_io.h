#ifndef DEV_IO_H
#define DEV_IO_H

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_GPIO_PINS 16
#define MAX_GPIO_PORT_NUM 3

int devWrite(int fd, char *ptr, int len);
int devRead(int fd, char *ptr, int len);


#ifdef __cplusplus
}
#endif
#endif
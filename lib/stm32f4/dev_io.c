#include "dev_io.h"
#include "debug.h"
#include "io_hw_proxy.h"
#include <errno.h>
#include <stddef.h>
#include <stm32f4xx.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_i2c.h>
#include <stm32f4xx_spi.h>
#include <stm32f4xx_usart.h>

/**
 * @brief
 *
 * @param fd
 * @param ptr
 * @param len
 * @return int
 */
int devWrite(int fd, char *ptr, int len) {

    int err = 0;
    // Handle HW interface write requests
    // TODO: Implement mutexes for each device descriptor
    switch (fd) {
    case GPIO_DEV_FILENO: {
        io_hw_proxy_desc_t *dsc = TO_IO_HW_PROXY_DESC_P(ptr);

        DEBUG_MSG(
            "GPIO write IO request: gpio_dsc->dev_id:%ld gpio_dsc->pin:%ld "
            "gpio_dsc->port:%ld\n",
            dsc->devData.gpioDevData.devId,
            dsc->devData.gpioDevData.pin,
            dsc->devData.gpioDevData.port);
    } break;

    case UART_DEV_FILENO:
        DEBUG_MSG("UART write IO request\n");
        break;

    case SPI_DEV_FILENO:
        DEBUG_MSG("SPI write IO request\n");
        break;

    case I2C_DEV_FILENO:
        DEBUG_MSG("I2C write IO request\n");
        break;

    default:
        err = ENODEV;
        break;
    }
    return err;
}

/**
 * @brief
 *
 * @param fd
 * @param ptr
 * @param len
 * @return int
 */
int devRead(int fd, char *ptr, int len) {
    int err = 0;
    // Handle HW interface read requests
    switch (fd) {
    case GPIO_DEV_FILENO:
        printf("[DEBUG]:" "GPIO read IO request\n");
        break;

    case UART_DEV_FILENO:
        DEBUG_MSG("UART read IO request\n");
        break;

    case SPI_DEV_FILENO:
        DEBUG_MSG("SPI read IO request\n");
        break;

    case I2C_DEV_FILENO:
        DEBUG_MSG("I2C read IO request\n");
        break;

    default:
        err = ENODEV;
        break;
    }
    return err;
}
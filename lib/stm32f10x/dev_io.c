#include "dev_io.h"
#include "FreeRTOS.h"
#include "debug.h"
#include "io_hw_proxy.h"
#include "stm32f10x_conf.h"
#include "task.h"
#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
/**
 * @brief
 *
 * @param fd
 * @param ptr
 * @param len
 * @return int
 */
int devWrite(int fd, const char *ptr, int len) {
    int err = 0;
    // Handle HW interface write requests

    io_hw_proxy_desc_t *dsc = TO_IO_HW_PROXY_DESC_P(ptr);

    switch (fd) {
    case GPIO_DEV_FILENO: {

        if ((dsc->devData.gpioDevData.devId > _GPIOG) ||
            (dsc->devData.gpioDevData.pin >= MAX_GPIO_PINS)) {
            err = -EINVAL;
            break;
        }

        const uint32_t GPIO_REG_SZ = GPIOB_BASE - GPIOA_BASE;

        GPIO_TypeDef *port =
            (GPIO_TypeDef *)(GPIOA_BASE +
                             (GPIO_REG_SZ * dsc->devData.gpioDevData.devId));

        if (dsc->devData.gpioDevData.portVal) {
            GPIO_Write(port, dsc->devData.gpioDevData.portVal);
        } else {
            GPIO_WriteBit(port,
                          (1 << dsc->devData.gpioDevData.pin),
                          dsc->devData.gpioDevData.pinVal);
        }
    } break;

    case UART_DEV_FILENO:
        break;

    case SPI_DEV_FILENO: {
        SPI_TypeDef *spi = NULL;

        if (dsc->data == NULL || dsc->dataLen == 0) {
            return -EINVAL;
        }

        if (dsc->devData.spiDevData.devId == _SPI1) {
            spi = SPI1;
        }

        if (dsc->devData.spiDevData.devId == _SPI2) {
            spi = SPI2;
        }

        if (dsc->devData.spiDevData.devId == _SPI3) {
            spi = SPI3;
        }

        // Check whether SPIx is enabled
        if ((spi->CR1 & (1 << 6)) == false || spi == NULL) {
            return -EIO;
        }

        size_t i                   = 0;
        const uint32_t GPIO_REG_SZ = GPIOB_BASE - GPIOA_BASE;

        GPIO_TypeDef *port =
            (GPIO_TypeDef *)(GPIOA_BASE +
                             (GPIO_REG_SZ * dsc->devData.spiDevData.csPort));

        bool assertCS = dsc->devData.spiDevData.assertCS;

        if (assertCS) {
            GPIO_WriteBit(port, (1 << dsc->devData.spiDevData.csPin), false);
        }

        err = dsc->dataLen;

        uint32_t startTime = xTaskGetTickCount();

        while (i < dsc->dataLen) {
            uint32_t timeDiff = xTaskGetTickCount() - startTime;

            if (timeDiff > dsc->timeoutMs) {
                err = -EIO;
                break;
            }

            if (SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET)
                continue;

            SPI_I2S_SendData(spi, dsc->data[i++]);
        }

        while (SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_TXE) == RESET)
            ;

        if (assertCS) {
            GPIO_WriteBit(port, (1 << dsc->devData.spiDevData.csPin), true);
        }
    } break;

    case I2C_DEV_FILENO:
        break;

    default:
        err = -ENODEV;
        break;
    }
    return err;
}

/**
 * @brief
 *
 * @param fd
 * @param ptr
 * @param len
 * @return int
 */
int devRead(int fd, char *ptr, int len) {
    int err = 0;
    // Handle HW interface read requests

    io_hw_proxy_desc_t *dsc = TO_IO_HW_PROXY_DESC_P(ptr);

    switch (fd) {
    case GPIO_DEV_FILENO: {

        if ((dsc->devData.gpioDevData.devId > _GPIOG) ||
            (dsc->devData.gpioDevData.pin >= MAX_GPIO_PINS)) {
            return -EINVAL;
        }

        if (dsc->data == NULL || dsc->dataLen < sizeof(uint16_t)) {
            return -EINVAL;
        }

        const uint32_t GPIO_REG_SZ = GPIOB_BASE - GPIOA_BASE;

        GPIO_TypeDef *port =
            (GPIO_TypeDef *)(GPIOA_BASE +
                             (GPIO_REG_SZ * dsc->devData.gpioDevData.devId));

        *((uint16_t *)dsc->data) = GPIO_ReadInputData(port);

        return sizeof(uint16_t);
    } break;

    case UART_DEV_FILENO:
        break;

    case SPI_DEV_FILENO: {
        uint32_t startTime = xTaskGetTickCount();
        SPI_TypeDef *spi   = NULL;

        if (dsc->data == NULL || dsc->dataLen == 0) {
            return -EINVAL;
        }

        if (dsc->devData.spiDevData.devId == _SPI1)
            spi = SPI1;

        if (dsc->devData.spiDevData.devId == _SPI2)
            spi = SPI2;

        if (dsc->devData.spiDevData.devId == _SPI3)
            spi = SPI3;

        size_t i = 0;

        bool assertCS = dsc->devData.spiDevData.assertCS;

        if (assertCS) {
            // TODO : Assert CS pin when needed
            const uint32_t GPIO_REG_SZ = GPIOB_BASE - GPIOA_BASE;

            GPIO_TypeDef *port =
                (GPIO_TypeDef *)(GPIOA_BASE + (GPIO_REG_SZ *
                                               dsc->devData.spiDevData.csPort));

            GPIO_WriteBit(port, (1 << dsc->devData.spiDevData.csPin), false);
        }

        err = dsc->dataLen;

        while (i < dsc->dataLen) {
            uint32_t timeDiff = xTaskGetTickCount() - startTime;

            if (timeDiff > dsc->timeoutMs) {
                err = -EIO;
                break;
            }

            if (SPI_I2S_GetFlagStatus(spi, SPI_I2S_FLAG_RXNE) == RESET)
                continue;

            dsc->data[i] = SPI_I2S_ReceiveData(spi);
            i++;
        }

        if (assertCS) {
            // TODO : Assert CS pin when needed
            const uint32_t GPIO_REG_SZ = GPIOB_BASE - GPIOA_BASE;

            GPIO_TypeDef *port =
                (GPIO_TypeDef *)(GPIOA_BASE + (GPIO_REG_SZ *
                                               dsc->devData.spiDevData.csPort));

            GPIO_WriteBit(port, (1 << dsc->devData.spiDevData.csPin), true);
        }

    } break;

    case I2C_DEV_FILENO:
        break;

    default:
        err = -ENODEV;
        break;
    }
    return err;
}

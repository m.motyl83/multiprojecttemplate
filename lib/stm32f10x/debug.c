#include "debug.h"
#include "fifo.h"
#include "stm32f10x_conf.h"
#include "stm32f10x_it.h"
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef ENABLE_UART_DEBUG

#define UART_FIFO_SIZE 128
#define DEFAULT_UART_SPEED 115200

#ifndef UART_DEBUG_SPEED
#pragma message("Setting UART_DEBUG_SPEED to 115200 bps")
#define UART_DEBUG_SPEED DEFAULT_UART_SPEED
#endif

#ifdef UART_USE_INTERRUPT
static fifo_t uart_fifo_rx;
static fifo_t uart_fifo_tx;
static char rxBuff[UART_FIFO_SIZE];
static char txBuff[UART_FIFO_SIZE];
#endif

/**
 * @brief
 *
 * @param ch
 */
void __io_putchar(char ch) {
#ifdef UART_USE_INTERRUPT

    while (1) {
        int len = fifo_write(&uart_fifo_tx, &ch, 1);

        if (len)
            break;
    }

    if ((USART1->CR1 & USART_CR1_TE) == false) {

        USART1->CR1 |= USART_CR1_TE;
    }

#else
    USART_SendData(USART1, ch);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) {
    }
#endif
}

/**
 * @brief
 *
 * @return char
 */
char __io_getchar() {
#ifdef UART_USE_INTERRUPT
    char tmp = 0;

    fifo_read(&uart_fifo_rx, &tmp, 1);

    return tmp;
#else
    while ((USART1->SR & USART_FLAG_RXNE) == (uint16_t)RESET) {
    }
    return (char)(USART1->DR & (uint16_t)0x01FF);
#endif
}

/**
 * @brief USART1_Init
 */
void USART1_Init(void) {

    /* Enable clock for USART1, AFIO and GPIOA */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_AFIO |
                               RCC_APB2Periph_GPIOA,
                           ENABLE);

    /* Enable USART1 */

    /* Enable USART1 */
    USART_Cmd(USART1, ENABLE);

    GPIO_InitTypeDef gpioa_init_struct;
    /* GPIOA PIN9 alternative function Tx */
    gpioa_init_struct.GPIO_Pin   = GPIO_Pin_9;
    gpioa_init_struct.GPIO_Speed = GPIO_Speed_50MHz;
    gpioa_init_struct.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &gpioa_init_struct);
    /* GPIOA PIN9 alternative function Rx */
    gpioa_init_struct.GPIO_Pin  = GPIO_Pin_10;
    gpioa_init_struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &gpioa_init_struct);

    USART_InitTypeDef usart1_init_struct;
    USART_StructInit(&usart1_init_struct);

    usart1_init_struct.USART_BaudRate   = UART_DEBUG_SPEED;
    usart1_init_struct.USART_WordLength = USART_WordLength_8b;
    usart1_init_struct.USART_StopBits   = USART_StopBits_1;
    usart1_init_struct.USART_Parity     = USART_Parity_No;
    usart1_init_struct.USART_Mode       = USART_Mode_Tx | USART_Mode_Rx;
    usart1_init_struct.USART_HardwareFlowControl =
        USART_HardwareFlowControl_None;
    /* Configure USART1 */

    USART_Init(USART1, &usart1_init_struct);

    USART_Cmd(USART1, ENABLE);

#ifdef UART_USE_INTERRUPT

    fifo_init(&uart_fifo_rx, rxBuff, UART_FIFO_SIZE);
    fifo_init(&uart_fifo_tx, txBuff, UART_FIFO_SIZE);

    USART1->CR1 &= ~USART_CR1_TE;

    /* Enable RXNE interrupt */
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART1, USART_IT_TC, ENABLE);

    /* Enable USART1 global interrupt */
    NVIC_EnableIRQ(USART1_IRQn);
#endif
}

#ifdef UART_USE_INTERRUPT
/**
 * @brief USART1_IRQHandler
 */
void USART1_IRQHandler(void) {
    if (USART_GetITStatus(USART1, USART_IT_RXNE)) {
        USART_ClearITPendingBit(USART1, USART_IT_RXNE);
        uint16_t rxData = USART1->DR;
        fifo_write(&uart_fifo_rx, &rxData, 1);
    }

    if (USART_GetITStatus(USART1, USART_IT_TC)) {
        USART_ClearITPendingBit(USART1, USART_IT_TC);
        char tmp = 0;
        if (fifo_read(&uart_fifo_tx, &tmp, 1)) {
            USART_SendData(USART1, tmp);
        } else {
            USART1->CR1 &= ~USART_CR1_TE;
        }
    }
}
#endif

/**
 * @brief
 *
 */
void USARTdebugInit(void) {
    USART1_Init();
}

/**
 * @brief readLine
 * @return
 */
char *readLine() {

#ifdef UART_USE_INTERRUPT
// while (!fifo_empty(rxFifo)) {
// }
#else
    while (1) {
    }
#endif

    return NULL;
}

#endif
typedef unsigned long addr_t;
#include <ctype.h>

/**
 * @brief
 *
 * @param ptr
 * @param len
 */
void hexdump(const void *ptr, size_t len) {
    addr_t address = (addr_t)ptr;
    size_t count;
    int i;

    for (count = 0; count < len; count += 16) {
        printf("0x%08lx: ", address);
        printf("%04lx %04lx %04lx %04lx |",
               *(const uint32_t *)address,
               *(const uint32_t *)(address + 4),
               *(const uint32_t *)(address + 8),
               *(const uint32_t *)(address + 12));
        for (i = 0; i < 16; i++) {
            char c = *(const char *)(address + i);
            if (isalpha(c)) {
                printf("%c", c);
            } else {
                printf(".");
            }
        }
        printf("|\r\n");
        address += 16;
    }
}

/**
 * @brief
 *
 * @param ptr
 * @param len
 */
void hexdump8(const void *ptr, size_t len) {
    addr_t address = (addr_t)ptr;
    size_t count;
    int i;

    for (count = 0; count < len; count += 16) {
        printf("0x%08lx: ", address);
        for (i = 0; i < 16; i++) {
            printf("0x%02x ", *(const uint8_t *)(address + i));
        }
        printf("\r\n");
        address += 16;
    }
}

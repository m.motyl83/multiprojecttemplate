#include "debug.h"
#include "stm32f10x.h"
#include "core_cm3.h"
#include "fifo.h"
#include "global_defines.h"

void SWO_Init(uint32_t portBits, uint32_t cpuCoreFreqHz) {
  uint32_t SWOSpeed = 3000000; /* default 64k baud rate */
  uint32_t SWOPrescaler = (cpuCoreFreqHz / SWOSpeed) -
                          1; /* SWOSpeed in Hz, note that cpuCoreFreqHz is
                                expected to be match the CPU core clock */

  /* enable trace in core debug */
  CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;

  /* "Selected PIN Protocol
Register": Select which
protocol to use for trace
output (2: SWO NRZ, 1: SWO
Manchester encoding) */
  *((volatile unsigned *)(ITM_BASE + 0x400F0)) = 0x00000002;

  /* "Async Clock Prescaler
                         Register". Scale the
                         baud rate of the
                         asynchronous output */
  *((volatile unsigned *)(ITM_BASE + 0x40010)) = SWOPrescaler;

  /* ITM Lock Access Register, C5ACCE55 enables more write
     access to Control Register 0xE00 :: 0xFFC*/
  *((volatile unsigned *)(ITM_BASE + 0x00FB0)) = 0xC5ACCE55;

  /* ITM Trace Control Register */
  ITM->TCR = ITM_TCR_TraceBusID_Msk | ITM_TCR_SWOENA_Msk | ITM_TCR_SYNCENA_Msk |
             ITM_TCR_ITMENA_Msk;

  /* ITM Trace Privilege Register */
  ITM->TPR = ITM_TPR_PRIVMASK_Msk;
  ITM->TER = portBits; /* ITM Trace Enable Register. Enabled tracing on
                          stimulus ports. One bit per stimulus port. */
  *((volatile unsigned *)(ITM_BASE + 0x01000)) = 0x400003FE; /* DWT_CTRL */
  *((volatile unsigned *)(ITM_BASE + 0x40304)) =
      0x00000100; /* Formatter and Flush Control Register */
}

/*!
 * \brief Sends a character over the SWO channel
 * \param c Character to be sent
 * \param portNo SWO channel number, value in the range of 0 to 31
 */
void SWO_PrintChar(char c, uint8_t portNo) {
  volatile int timeout;

  /* Check if Trace Control Register (ITM->TCR at 0xE0000E80) is set */
  if ((ITM->TCR & ITM_TCR_ITMENA_Msk) ==
      0) {  /* check Trace Control Register if ITM trace is enabled*/
    return; /* not enabled? */
  }
  /* Check if the requested channel stimulus port (ITM->TER at 0xE0000E00) is
   * enabled */
  if ((ITM->TER & (1ul << portNo)) ==
      0) {  /* check Trace Enable Register if requested port is enabled */
    return; /* requested port not enabled? */
  }
  timeout = 5000; /* arbitrary timeout value */
  while (ITM->PORT[0].u32 == 0) {
    /* Wait until STIMx is ready, then send data */
    timeout--;
    if (timeout == 0) {
      return; /* not able to send */
    }
  }
  ITM->PORT[0].u16 = 0x08 | (c << 8);
}

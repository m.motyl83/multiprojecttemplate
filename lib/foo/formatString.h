#ifndef FORMATSTRING_H
#define FORMATSTRING_H

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef size_t rsize_t;

bool formatString(char *s, rsize_t n, const char *format, ...);

#ifdef __cplusplus
}
#endif
#endif
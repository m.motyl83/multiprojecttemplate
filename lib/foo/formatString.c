#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "formatString.h"
typedef size_t rsize_t;

static void strrev(char *str, int maxLen) {
    int i;
    int j;

    unsigned len = strnlen((const char *)str, maxLen);
    for (i = 0, j = len - 1; i < j; i++, j--) {
        unsigned char a = str[i];
        str[i]          = str[j];
        str[j]          = a;
    }
}

/**
 * @brief
 *
 * @param num
 * @param str
 * @param len
 * @param base
 * @return int
 */
static int itoa(unsigned num, char *str, int len, int base) {
    unsigned sum = num;
    int i        = 0;

    if (len == 0)
        return -1;

    do {
        unsigned digit = sum % base;
        if (digit < 0xA)
            str[i++] = '0' + digit;
        else
            str[i++] = 'A' + digit - 0xA;
        sum /= base;
    } while (sum && (i < (len - 1)));

    if (i == (len - 1) && sum) {
        return -1;
    }

    str[i] = '\0';
    strrev(str, len);

    return i;
}

/**
 * @brief
 *
 * @param s
 * @param n
 * @param format
 * @param ...
 * @return true
 * @return false
 */
bool formatString(char *s, rsize_t n, const char *format, ...) {
    const size_t RSIZE_MAX = (SIZE_MAX >> 1);

    if (s == NULL || n == 0 || n > RSIZE_MAX) {
        return false;
    }

    if (format == NULL) {
        s[0] = '\0';
        return false;
    }

    size_t i        = 0;
    const char *tmp = format;

    va_list ap;
    va_start(ap, format);

    while (*tmp != '\0' && (n > 0)) {
        if (*tmp == '%') {
            tmp++;

            if (*tmp == '%') {
                s[i++] = *tmp++;
                n--;
                continue;
            }

            char c       = tolower(*tmp++);
            int convBase = -1;

            switch (c) {
            case 'x': {
                convBase = 16;
            } break;

            case 'd': {
                convBase = 10;
            } break;

            case 's': {
                const char *ss = va_arg(ap, char *);

                if (ss == NULL) {
                    s[0] = '\0';
                    va_end(ap);
                    return false;
                }

                int j = 0;
                while ((c = ss[j]) != '\0' && (n > 0)) {
                    s[i++] = ss[j];
                    n--;
                    j++;
                }
                continue;
            }

            default: {
                s[0] = '\0';
                va_end(ap);
                return false;
            }
            }

            int p = va_arg(ap, int);

            // itoa return number written bytes, -1 in case of error
            int res = itoa((unsigned int)p, &s[i], n, convBase);

            if (res < 0) {
                s[0] = '\0';
                va_end(ap);
                return false;
            }

            // update number bytes written to the buffer and remaining bytes in
            // buffer
            n = n - res;
            i = i + res;
        } else {
            s[i++] = *tmp++;
            n--;
        }
    }

    va_end(ap);

    // Ad string terminator
    n == 0 ? (s[i - 1] = '\0') : (s[i] = '\0');

    tmp = NULL;

    return true;
}

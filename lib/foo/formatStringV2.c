#include "formatStringV2.h"
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static bool strrev(char *str, int maxLen) {
    int i;
    int j;
    bool res = false;

    unsigned len = strnlen((const char *)str, maxLen);

    if (len == 0)
        return res;

    for (i = 0, j = len - 1; i < j; i++, j--) {
        unsigned char a = str[i];
        str[i]          = str[j];
        str[j]          = a;
    }

    res = true;
    return res;
}

/**
 * @brief
 *
 * @param num
 * @param str
 * @param len
 * @param base
 * @return int
 */
static int itoa(unsigned num, char *str, size_t len, unsigned char base) {
    unsigned sum = num;
    size_t i     = 0;

    if (len == 0 || base == 0 || str == NULL) {
        return -1;
    }

    do {
        unsigned digit = sum % base;
        if (digit < 0xA)
            str[i++] = '0' + digit;
        else
            str[i++] = 'A' + digit - 0xA;
        sum /= base;
    } while (sum && (i < (len - 1)));

    if (i == (len - 1) && sum) {
        return -1;
    }

    bool res = strrev(str, len);

    if (res == false) {
        i = 0;
    }

    str[i] = '\0';

    return i;
}

/**
 * @brief
 *
 * @param s - output buffer
 * @param n - size of output buffer
 * @param fmt - format params
 * @param fmt_sz - size of fmt array
 * @return true
 * @return false
 */
bool formatStringV2(char *s, rsize_t n, const format_t *fmt, size_t fmt_sz) {
    const size_t RSIZE_MAX = (SIZE_MAX >> 1);

    if (s == NULL || n == 0 || n > RSIZE_MAX) {
        return false;
    }

    if (fmt == NULL || fmt_sz == 0) {
        s[0] = '\0';
        return false;
    }

    size_t i = 0;
    size_t j = 0;

    while (j < fmt_sz && (n > 0)) {
        if (fmt[j].msg) {
            // Copy string to the buffer
            size_t strLen = strnlen(fmt[j].msg, n);

            strncpy(&s[i], fmt[j].msg, strLen);

            n = n - strLen;
            i = i + strLen;
        }

        if (fmt[j].base != BASE_NONE) {
            int res = itoa((unsigned int)fmt[j].input, &s[i], n, fmt[j].base);

            if (res < 0) {
                s[0] = '\0';
                return false;
            }

            n = n - res;
            i = i + res;
        }

        j++;
    }

    // Add string terminator
    n == 0 ? (s[i - 1] = '\0') : (s[i] = '\0');

    return true;
}
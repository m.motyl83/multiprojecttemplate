#ifndef FORMATSTRINGV2_H
#define FORMATSTRINGV2_H

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef size_t rsize_t;

typedef enum { BASE_DECIMAL = 10, BASE_HEX = 16, BASE_NONE } conv_base_t;

typedef struct format {
    const char *msg;
    uint32_t input;
    conv_base_t base;
} format_t;

bool formatStringV2(char *s, rsize_t n, const format_t *fmt, size_t fmt_sz);

#ifdef __cplusplus
}
#endif

#endif
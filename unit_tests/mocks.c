
#include "dev_io.h"
#include "io_hw_proxy.h"
#include <CppUTest/TestHarness_c.h>
#include <CppUTest/UtestMacros.h>
#include <CppUTestExt/MockSupport_c.h>
#include <fcntl.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int devWrite(int fd, char *ptr, int len) {
    // Handle HW interface write requests
    mock_c()->actualCall(__FUNCTION__);

  //  if (mock_c()->hasReturnValue()) {
   //     printf("foooo\n");
   // //     return mock_c()
    //         ->actualCall(__FUNCTION__)
    //         ->intReturnValue();
    // } else {
    //     mock_c()->actualCall(__FUNCTION__);
  //  }

    return mock_c()->returnIntValueOrDefault(0);
}

int devRead(int fd, char *ptr, int len) {
    // Handle HW interface read requests
    return mock_c()->actualCall(__FUNCTION__)->withOutputParameter("ptr", ptr)->returnIntValueOrDefault(0);

    // if (mock_c()->hasReturnValue()) {
    //     return mock_c()
    //         ->actualCall(__FUNCTION__)
    //         ->withOutputParameter("ptr", ptr)
    //         ->intReturnValue();
    // } else {
    //     mock_c()->actualCall(__FUNCTION__);
    // }

    // return mock_c()->returnIntValueOrDefault(0);
}
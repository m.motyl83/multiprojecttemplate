#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTest/TestHarness.h"
#include "CppUTestExt/MockSupport.h"
#include "foo.h"
#include "io_hw_proxy.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

TEST_GROUP(AppTestGroup) {
    void setup(){
        // mock().installComparator("resp_t*", c1);
        // mock().installComparator("radioList*", c2);
        // mock().installComparator("size_t*", c3);
        // mock().installComparator("uart_port_t", c4);
        // mock().installComparator("gpio_config_t*", c5);

        // mock().installCopier("size_t*", mvc1);
    };
    void teardown() override {
        mock().removeAllComparatorsAndCopiers();
        mock().clear();
    };
};

TEST(AppTestGroup, test1) {

    bool res = foo();

    int i2c_fd = open("/dev/i2c", O_RDWR);

    if (i2c_fd != I2C_DEV_FILENO) {
        FAIL_TEST_LOCATION("Bad i2c device file", __FILE__, __LINE__);
    }

    char data[10] = {0x55};
    mock().expectOneCall("dev_write").andReturnValue(0);

    mock()
        .expectOneCall("dev_read")
        .andReturnValue(10)
        .withOutputParameterReturning("ptr", data, 10);

    uint8_t testData[10];
    io_hw_proxy_desc dsc = {0};
    dev_i2c_desc_data_t i2c;
    i2c.ifaceNum  = 1;
    i2c.slaveAddr = 0xA0;
    dsc.data       = testData;
    dsc.dataLen    = 10;

    int res2 = write(i2c_fd, &dsc, sizeof(io_hw_proxy_desc_t));
    printf("res2:%d\n", res2);

    res2 = read(i2c_fd, &dsc, sizeof(io_hw_proxy_desc_t));

    CHECK_FALSE(res);
}

int main(int argc, char const *argv[]) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}

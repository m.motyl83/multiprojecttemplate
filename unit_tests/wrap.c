
#include "dev_io.h"
#include "io_hw_proxy.h"
#include <CppUTest/TestHarness_c.h>
#include <CppUTest/UtestMacros.h>
#include <CppUTestExt/MockSupport_c.h>
#include <fcntl.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int __real_write(int file, char *ptr, int len);
int __real_read(int file, char *ptr, int len);
int __real_open(const char *name, int flags, int mode);

/**
 * @brief
 *
 * @param file
 * @param ptr
 * @param len
 * @return int
 */
int __wrap_write(int file, char *ptr, int len) {

    switch (file) {
    case GPIO_DEV_FILENO:
    case I2C_DEV_FILENO:
    case SPI_DEV_FILENO:
    case UART_DEV_FILENO: {
        return devWrite(file, ptr, len);
    } break;

    default:
        return __real_write(file, ptr, len);
    }
}

/**
 * @brief
 *
 * @param file
 * @param ptr
 * @param len
 * @return int
 */
int __wrap_read(int file, char *ptr, int len) {

    switch (file) {
    case GPIO_DEV_FILENO:
    case I2C_DEV_FILENO:
    case SPI_DEV_FILENO:
    case UART_DEV_FILENO: {
        return devRead(file, ptr, len);
    } break;

    default:
        return __real_read(file, ptr, len);
    }
}

/**
 * @brief
 *
 * @param name
 * @param flags
 * @param mode
 * @return int
 */
int __wrap_open(const char *name, int flags, int mode) {
    size_t nameLen = strnlen(name, 16);

    if (strncmp(name, "/dev/spi", nameLen) == 0) {
        return SPI_DEV_FILENO;
    } else {
        if (strncmp(name, "/dev/i2c", nameLen) == 0) {
            return I2C_DEV_FILENO;
        } else {
            if (strncmp(name, "/dev/gpio", nameLen) == 0) {
                return GPIO_DEV_FILENO;
            } else {
                if (strncmp(name, "/dev/uart", nameLen) == 0) {
                    return UART_DEV_FILENO;
                }
            }
        }
    }

    return __real_open(name, flags, mode);
}
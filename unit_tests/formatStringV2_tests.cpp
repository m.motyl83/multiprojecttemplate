#include "CppUTest/CommandLineTestRunner.h"
#include "CppUTest/TestHarness.h"
#include "formatStringV2.h"

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof((x)[0]))

TEST_GROUP(formatStringV2TestsGroup){};

TEST(formatStringV2TestsGroup, formatStringV2_simple_test) {

    char b[16] = {0};

    const format_t f = {.msg = "foo", .base = BASE_NONE};

    bool res = formatStringV2(b, 15, &f, 1);

    CHECK_TRUE(res);
    STRCMP_EQUAL_TEXT("foo", b, "Comparing output buffer ..");
}

TEST(formatStringV2TestsGroup, formatStringV2_full_buffer) {

    char b[4] = {0};

    const format_t f = {.msg = "foo1", .input = 10, .base = BASE_DECIMAL};

    bool res = formatStringV2(b, 4, &f, 1);

    CHECK_FALSE(res);
}

TEST(formatStringV2TestsGroup, formatStringV2_full_buffer2) {

    char b[4] = {0};

    const format_t f[] = {{.msg = "foo1", .base = BASE_NONE},
                          {.msg = "foo1", .base = BASE_NONE}};

    bool res = formatStringV2(b, 4, f, 2);

    CHECK_TRUE(res);
}

TEST(formatStringV2TestsGroup, formatStringV2_full_buffer3) {

    char b[16] = {0};

    const format_t f[] = {{.msg = "fo", .base = BASE_NONE},
                          {.msg = "foo1", .input = 10, .base = BASE_DECIMAL}};

    bool res = formatStringV2(b, 16, f, ARRAY_LENGTH(f));

    CHECK_TRUE(res);
}

TEST(formatStringV2TestsGroup, formatStringV2_unterminated_string_failed) {

    char b[16] = {0};
    char s[4]  = {0};

    const format_t f = {.msg = s, .base = BASE_NONE};

    s[0] = 'f';
    s[1] = 'o';
    s[2] = 'o';
    s[3] = 'o';

    bool res = formatStringV2(b, 16, &f, 1);

    CHECK_TRUE(res);
}

TEST(formatStringV2TestsGroup, formatStringV2_null_msg_failed) {

    char b[16] = {0};

    const format_t f = { .msg = NULL, .base = BASE_NONE};

    bool res = formatStringV2(b, 15, &f, 1);

    CHECK_TRUE(res);
}
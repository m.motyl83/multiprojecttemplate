
# Multi project template

## About

This sample project/template shows integration following components in single Cmake project:

- native C/C++ aplications
- STM32F401CCU6 sample application using freeRTOS
- unit tests with code coverage
- cppcheck html report
- IO HW proxy layer POC which aims to separate application bussines logic from hardware.<br>
  The concept is based on **open, read, write and fcntl** C system calls to communicate with hardware using IO descriptors.
  This let unit test application logic and being platform-agnostic.<br>

Project structure:

```bash
    ├── apps
    │   ├── app_c
    │   ├── app_cpp
    │   └── arm_c_app
    ├── cmake
    ├── doc
    │   ├── coverage_report
    │   └── cppcheck_report
    ├── include  
    ├── linker
    ├── openocd
    ├── src
    │   ├── foo
    │   ├── freeRTOS
    │   └── stm32f4
    └── unit_tests
        └── include
```

## IO HW PROXY design/flow

![IO HW PROXY design/flow](/doc/IO_HW_PROXY.png)

## IO HW PROXY UT(unit tests) design/flow

![IO HW PROXY design/flow](/doc/IO_HW_PROXY_UT.png)

## Building

Building native C/C++ app :

- `mkdir ./build`
- `cd build`
- `cmake .. <cmake options>`
- `make`

Building ARM CORTEXM4 app:

- `mkdir ./build_arm_app`
- `cd build_arm_app`
- `cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=../cmake/stm32Toolchain.cmake -DENABLE_UART_DEBUG=True -DUART_USE_INTERRUPT=True -DCPU_TYPE=cortex-m4`
- `make arm_c_app`
Building ARM CORTEX M3 F10X FAMILY app:

- `mkdir ./build_arm_f10x_app`
- `cd build_arm_f10x_app`
- `cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=../cmake/stm32Toolchain.cmake -DENABLE_UART_DEBUG=True -DUART_USE_INTERRUPT=True -DCPU_TYPE=cortex-m3`
- `make arm_c_app_f103`
  
Please note that building ARM C application requires explicit make target selection.

Building unit tests:

- `mkdir ./build_ut`
- `cd build_ut`
- `make app_unit_tests && make run_unit_tests`

Please note that building and runnig unit tests requires explicit make target selection.

## Remarks
Additional work should be done to add mutexes for low level operations


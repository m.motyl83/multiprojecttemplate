#ifndef IO_HW_PROXY_H
#define IO_HW_PROXY_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum hw_io_fileno {
    // Dev numbers 0-2 are reserverd for stdout,in and err
    GPIO_DEV_FILENO = 3,
    I2C_DEV_FILENO,
    UART_DEV_FILENO,
    SPI_DEV_FILENO
};

typedef enum subDevId{
    _GPIOA = 0,
    _GPIOB,
    _GPIOC,
    _GPIOD,
    _GPIOE,
    _GPIOF,
    _GPIOG,
    _UART0,
    _USART1,
    _USART2,
    _USART3,
    _USART4,
    _USART5,
    _SPI1,
    _SPI2,
    _SPI3,
    _I2C1,
    _I2C2
}subDevId_t;

typedef enum hw_dev { DEV_GPIO, DEV_I2C, DEV_UART, DEV_SPI } hw_dev_t;

typedef struct dev_gpio_desc_data {
    subDevId_t devId;
    uint8_t pinVal;
    uint8_t pin;
    uint16_t portVal;
} dev_gpio_desc_data_t;

typedef struct dev_i2c_desc_data {
    subDevId_t devId;
    uint8_t slaveAddr;
} dev_i2c_desc_data_t;

typedef struct dev_uart_desc_data {
    subDevId_t devId;
} dev_uart_desc_data_t;

typedef struct dev_spi_desc_data {
    subDevId_t devId;
    subDevId_t csPort;
    uint8_t csPin;
    uint8_t assertCS;
} dev_spi_desc_data_t;

typedef struct io_hw_proxy_desc {
    uint8_t *data; //Pointer to buffer
    size_t dataLen;//Buffer size

    void *usrData;
    size_t usrDataLen;

    uint32_t timeoutMs;

    union io_hw_proxy_dev_data {
        dev_gpio_desc_data_t gpioDevData;
        dev_i2c_desc_data_t i2cDevData;
        dev_uart_desc_data_t uartDevData;
        dev_spi_desc_data_t spiDevData;
    } devData;

} io_hw_proxy_desc_t;

#define TO_IO_HW_PROXY_DESC_P(p) ((io_hw_proxy_desc_t *)p)

#ifdef __cplusplus
}
#endif

#endif
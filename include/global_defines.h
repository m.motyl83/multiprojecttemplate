#define ENABLE_UART_DEBUG
/* #undef UART_DEBUG_SPEED */
#define UART_USE_INTERRUPT
#define F_CPU              (72000000)
#define __STACK_SIZE       (2048)
#define __HEAP_SIZE        (4096)

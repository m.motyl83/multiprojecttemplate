import socket
import sys
import time
import subprocess

class openocd_rw:

    def __init__(self):
        pass

    def exit(self):
        self.s.close()

    def init(self, addr="localhost", port=4444):
        self.addr = addr
        self.port = port
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.timeout = 4.0
        self.init_status = False

        # Check if OpenOCD server is not running
        try:
            self.s.connect(("localhost", 4444))
        except ConnectionRefusedError:
            print("ConnectionRefusedError")
            cmd = "xmessage \"openocd server is not running\nPlease run openocd -f openocd/debug.cfg first\" -nearmouse -geometry 400x100+0+0"

            res = subprocess.check_call(cmd, shell=True)
            self.init_status = False
            return False

        self.init_status = True
        self.s.settimeout(self.timeout)
        self.s.setblocking(True)

        rx_buf = self.s.recv(1024)
        rx_buf = ""

    def __socket_send(self, cmd, wait_for_comletion=False, timeout=5.0):
        tx_buf = cmd
        rx_buf = bytes()
        timeout_step = 0.1

        if not tx_buf.endswith("\n"):
            tx_buf += "\n"

        tx_buf_bytes = bytes(tx_buf, encoding="utf-8")
        self.s.send(tx_buf_bytes)

        time.sleep(0.1)
        while True:
            try:
                rx_buf = self.s.recv(1024)
            except TimeoutError:
                print("TimeoutError")
                return None

            rx_stripped = rx_buf.strip(b' ')

            if rx_stripped.endswith(b'>'):
                return rx_stripped.decode("utf-8")
            else:
                if wait_for_comletion == False:
                    return None
                else:
                    rx_buf = b''
                    rx_stripped = b''

                    time.sleep(timeout_step)
                    timeout -= timeout_step

                    if timeout == 0:
                        print("Completion timout...")
                        return None
                    continue

    def __search_for_mem_value(self, rx_buff):

        splited_string = rx_buff.splitlines()

        for x in range(len(splited_string)):
            if splited_string[x].startswith('\x00'):

                val = splited_string[x][1:]
                val_int = int(val)

                return hex(val_int)

        return None

    def check_for_error(self, rx_buff):

        return False

    def readMem32(self, mam_addr: int):
        tmp = "mrw " + str(mam_addr)
        rx = self.__socket_send(tmp)

        if rx == None:
            return None

        return self.__search_for_mem_value(rx)

    def readMem8(self, mam_addr: int):
        tmp = "mrb " + str(mam_addr)
        rx = self.__socket_send(tmp)

        if rx == None:
            return None

        return self.__search_for_mem_value(rx)

    def halt(self):
        tmp = "halt"
        rx = self.__socket_send(tmp, True)

        if rx == None:
            return False

        return True

    def resume(self):
        tmp = "resume"
        rx = self.__socket_send(tmp, True)

        if rx == None:
            return False

        return True

    def reset(self, reset_mode=["run", "halt", "init"]):
        cmd = "reset " + reset_mode
        rx = self.__socket_send(cmd)

        if rx == None:
            return False

        return True

    def write_elf_image(self, file_to_write):

        cmd = "flash write_image erase unlock " + file_to_write + " 0 elf"
        print("flash cmd:")
        print(cmd)
        rx = self.__socket_send(cmd, True)

        if rx == None:
            print("rx == None")
            return False

        return True

    def verify_image(self, file_to_verify):
        cmd = "verify_image " + file_to_verify
        rx = self.__socket_send(cmd, True)

        if rx == None:
            print("rx == None")
            return False

        return True

    def erase_target(self):
        cmd = "stm32f4x mass_erase 0"
        rx = self.__socket_send(cmd, True)

        if rx == None:
            print("rx == None")
            return False

        return True

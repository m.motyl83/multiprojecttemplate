#!/usr/bin/env python3

import os
import time
import sys
from mmOpenOcd import openocd_rw


def flashTarget():
    print("[MM DEBUG] Flashing device...")
    sys.stdout.flush()
    # numArgs = len(sys.argv) - 1
    file_to_flash = sys.argv[1]

    x = openocd_rw()

    if x.init() == False:
        print("Init Error..")
        del x
        sys.exit(-1)

    x.halt()

    if x.write_elf_image(file_to_flash) == False:
        print("Flashing error..")
        sys.exit(-1)

    if x.verify_image(file_to_flash) == False:
        print("Verify error..")
        sys.exit(-1)

    x.reset(reset_mode="run")
    x.exit()

    del x
    print("[MM DEBUG] Flashing device...exit")
    sys.exit(0)


if __name__ == '__main__':
    flashTarget()

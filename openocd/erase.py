#!/usr/bin/env python3

import os
import time
import sys
from mmOpenOcd import openocd_rw


def eraseTarget():
    print("[MM DEBUG] Erasing device...")
    sys.stdout.flush()

    x = openocd_rw()

    if x.init() == False:
        print("Init Error..")
        del x
        sys.exit(-1)

    x.halt()
    x.erase_target()
    x.exit()

    del x
    sys.exit(0)


if __name__ == '__main__':
    eraseTarget()
